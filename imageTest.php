<?php
    $targetDir = "uploads/";
    $targetFile = $targetDir . basename($_FILES["fileToUpload"]["name"]);
    $uploadOk = 1;
    $imageType = strtolower(pathinfo($targetFile,PATHINFO_EXTENSION));
    if (isset($_POST["submit"])) {
        $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
        if($check !== FALSE) {
            echo "File looks like an image ". $check["mime"].".";
        }
        else {
            echo "File is not an image ";
            $uploadOk = 0;
        }

        //File exists
        if(file_exists($targetFile)) {
            echo "File already exists";
            $uploadOk = 0;
        }

        //file size
        if($_FILES["fileToUpload"]["size"] > 1000000) {
            echo "File too big";
            $uploadOk = 0;
        }

        //file type
        if($imageType != "jpg" && $imageType != "jpeg" && $imageType != "png" && $imageType != "gif") {
            echo "Only jpg,jpeg,png,gif allowed.";
        }

        if($uploadOk == 0 ) {
            echo "Fix problems before uploading thx";
        }
        elseif($uploadOk == 1) {
            if(move_uploaded_file($_FILES["fileToUpload"]["tmp_name"],$targetFile)) {
                echo "Success";
            }
            else {
                echo "Failed to upload";
            }
        }
     }

?>