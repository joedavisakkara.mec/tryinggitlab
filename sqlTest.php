<?php
    $servername = "localhost";
    $username="root";
    $password="qwe";
    $dbname="quizapp";
    
    $conn = new mysqli($servername,$username,$password,$dbname);

    if($conn->connect_error) {
        die("connection failed \n".$conn->connect_error);
    }
    
    $preparedStmt = $conn->prepare("INSERT INTO userinfo (firstname,lastname,username,password) VALUES (?,?,?,?)");
    $preparedStmt-> bind_param('ssss',$firstname,$lastname,$username,$password);

    $firstname = "Tim";
    $lastname = "Horton";
    $username = "timmyh";
    $password = "timmydabest";
    $preparedStmt->execute();

    $firstname = "Jim";
    $lastname = "Carlton";
    $username = "jimmyc";
    $password = "jimmydabest";
    $preparedStmt->execute();
    
    $firstname = "Cave";
    $lastname = "Johnson";
    $username = "sciencewiz";
    $password = "test";
    $preparedStmt->execute();

?>