<html>
	<head>
		<title>Testing stuff out</title>
	</head>
	<body>
		<?php
			session_start();
			$name=$email=$password=$random="";
			$nameErr=$emailErr=$passwordErr=$randomErr="";
			function prepare($data) {
				$data = trim($data);
				$data = stripslashes($data);
				$data = htmlspecialchars($data);
				return $data;
			}

			if(isset($_POST['submit'])) {
				//check if name is empty or has special characters
				if(empty($_POST["name"])) {
					$nameErr = "*Required field";
				}
				else {
					$name = prepare($_POST["name"]);
					if(preg_match('/[\\/~`\!@#\$%\^&\*\(\)_\-\+=\{\}\[\]\|;:"\<\>,\.\?\\\]/',$name)){
						$nameErr = "*Must not contain special characters other than '";
					};
				}

				//check if email is empty or is invalid
				if(empty($_POST["email"])) {
					$emailErr = "*Required field";
				}
				else {
					$email = prepare($_POST["email"]);
					if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
						$emailErr = "*Enter a valid email address";
					}
				}

				//check if password is empty or not long enough
				if(empty($_POST["password"])) {
					$passwordErr = "*Required field";
				}
				else {
					$password = prepare($_POST["password"]);
					if(strlen($password)<6) {
						$passwordErr = "*Must be at least 6 characters long";
					}
				}
				
				if(!empty($_POST["password"])) {
					$random = prepare($_POST["random"]);
					$_SESSION["animal"] = $random;
				}
			}
		?>
		<h2>Here I test</h2>
		<table>
			<form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
				<tr> 
					<td> *required field </td>
				</tr>
				<tr>	
					<td> *Name: </td>
					<td> <input type="text" name="name" value="<?php echo $name ?>"><?php echo $nameErr; ?> </td>
				</tr>
				<tr>	
					<td> *Email: </td>
					<td> <input type="text" name="email" value="<?php echo $email ?>"><?php echo $emailErr; ?> </td>
				</tr>
				<tr>	
					<td> *Password: </td> 
					<td> <input type="password" name="password" value="<?php echo $password ?>"><?php echo $passwordErr; ?> </td>
				</tr>
				<tr>	
					<td> Pick a random one of these </td>
					<td> <input type="radio" name ="random" value="cow" <?php if(isset($random) && $random == "cow") echo "checked" ?> >Cow </td>
				</tr>
				<tr>
					<td></td>
					<td> <input type="radio" name ="random" value="chicken" <?php if(isset($random) && $random == "chicken") echo "checked" ?> >Chicken </td>
				</tr>
				<tr>	
					<td></td>
					<td> <input type="radio" name ="random" value="horse" <?php if(isset($random) && $random == "horse") echo "checked" ?> >Horse </td>
				</tr>					
					<td></td>
					<td> <input type="radio" name ="random" value="racoon" <?php if(isset($random) && $random == "racoon") echo "checked" ?> >Racoon </td>	
				</tr>
				<tr>
					<td> <input type="submit" name="submit" value="Submit"> </td>
				</tr>	
			</form>
		</table>		
	</body>	
</html>


